`enum_delegate` changelog
====================

All user visible changes to `enum_delegate` crate will be documented in this file. This project uses [Semantic Versioning 2.0.0].




## [0.3.0] - 2022-??-??
[0.3.0]: /../../tree/v0.3.0

[Diff](/../../compare/v0.2.0...v0.3.0)

### BC Breaks

- Refactored project structure;
- Removed `cross_crate_example` crate;
- Removed `Into`/`TryFrom` implementations support in macro expansion, use [derive_more] crate instead;
- `benchmarks` crate replaced with benchmarks in `enum_delegate` crate;
- `enum_delegate_lib` create replaced with `enum_delegate_codegen` crate;
- `#[enum_delegate::register]` and `#[enum_delegate::implement]` macros are replaced with single `#[delegate]` macro.

### Added

- trait deriving through `#[enum_delegate::register]` macro;
- `fnv1a128`, `Bound`, `Convert`, `Either`, `Void` types in `enum_delegate` crate.

[derive_more]: https://docs.rs/derive_more/latest/derive_more




[Semantic Versioning 2.0.0]: https://semver.org

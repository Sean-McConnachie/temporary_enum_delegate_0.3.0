/// Non-cryptographic hash with good dispersion to use as a [`str`](prim@str) in
/// `const` generics. See [spec] for more info.
///
/// [spec]: https://datatracker.ietf.org/doc/html/draft-eastlake-fnv-17.html
#[must_use]
pub const fn fnv1a128(str: &str) -> u128 {
    const FNV_OFFSET_BASIS: u128 = 0x6c62_272e_07bb_0142_62b8_2175_6295_c58d;
    const FNV_PRIME: u128 = 0x0000_0000_0100_0000_0000_0000_0000_013b;

    let bytes = str.as_bytes();
    let mut hash = FNV_OFFSET_BASIS;
    let mut i = 0;
    while i < bytes.len() {
        hash ^= bytes[i] as u128;
        hash = hash.wrapping_mul(FNV_PRIME);
        i += 1;
    }
    hash
}

/// Enum for holding either `L` or `R` type.
#[derive(Clone, Copy, Debug)]
pub enum Either<L, R> {
    /// Left type.
    Left(L),

    /// Right type.
    Right(R),
}

/// Type of unreachable [`Either`] variant.
#[derive(Clone, Copy, Debug)]
pub enum Void {}

/// Trait for restricting delegation of a trait to the specified types only.
pub trait Bound<
    const PATH_HASH: u128,
    const TRAIT_NAME_HASH: u128,
    const LINE: u32,
    const COLUMN: u32,
>
{
}

/// Trait for converting a type into its delegate.
pub trait Convert {
    /// Type of an owned any enum variant.
    type Owned;

    /// Type of a referenced any enum variant.
    type Ref<'a>
    where
        Self: 'a;

    /// Type of a mutable referenced any enum variant.
    type RefMut<'a>
    where
        Self: 'a;

    /// Converts this enum into an owned variant.
    fn convert_owned(self) -> Self::Owned;

    /// Converts reference to this enum into a variant reference.
    fn convert_ref(&self) -> Self::Ref<'_>;

    /// Converts mutable reference to this enum into a mutable variant
    /// reference.
    fn convert_ref_mut(&mut self) -> Self::RefMut<'_>;
}

//! `#[delegate]` macro expansion on types (structs or enums).

use std::iter;

use itertools::Itertools as _;
use proc_macro2::{Span, TokenStream};
use quote::{quote, ToTokens};
use syn::{
    parse::{Parse, ParseStream},
    parse_quote,
    punctuated::Punctuated,
    spanned::Spanned,
    token,
};

use crate::MacroPath;

/// Arguments for `#[delegate]` macro expansion on types (structs or enums).
struct Args {
    /// `derive` attribute argument, specifying derived traits.
    derive: Punctuated<syn::Path, token::Comma>,
}

impl Parse for Args {
    fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
        if input.is_empty() {
            return Ok(Self {
                derive: Punctuated::new(),
            });
        }

        input
            .parse::<syn::Ident>()
            .ok()
            .filter(|i| i == "derive")
            .map(drop)
            .ok_or_else(|| {
                syn::Error::new(
                    input.span(),
                    "expected `derive` attribute argument",
                )
            })?;

        let args;
        let _ = syn::parenthesized!(args in input);
        Ok(Self {
            derive: Punctuated::parse_terminated(&args)?,
        })
    }
}

/// Definition of `#[delegate]` macro expansion on types (structs or enums).
#[derive(Debug)]
pub(crate) struct Definition {
    ident: syn::Ident,
    generics: syn::Generics,

    /// Delegated enum [`Variant`]s or a single struct [`Field`].
    delegated: DelegatedTypes,

    /// Traits to derive.
    derived_traits: Vec<syn::Path>,

    /// Path to the macro definitions.
    macro_path: MacroPath,
}

impl Definition {
    /// Parses [`Definition`] from a [`syn::ItemEnum`].
    pub(crate) fn parse_enum(
        item: syn::ItemEnum,
        args: TokenStream,
    ) -> syn::Result<Self> {
        let args = syn::parse2::<Args>(args)?;

        Ok(Self {
            ident: item.ident,
            generics: item.generics,
            delegated: DelegatedTypes::Variants(
                item.variants
                    .into_iter()
                    .map(TryInto::try_into)
                    .collect::<Result<_, _>>()?,
            ),
            derived_traits: args.derive.into_iter().collect(),
            macro_path: MacroPath::default(),
        })
    }

    /// Parses [`Definition`] from a [`syn::ItemStruct`].
    pub(crate) fn parse_struct(
        item: syn::ItemStruct,
        args: TokenStream,
    ) -> syn::Result<Self> {
        let args = syn::parse2::<Args>(args)?;

        Ok(Self {
            ident: item.ident,
            generics: item.generics,
            delegated: DelegatedTypes::Field(item.fields.try_into()?),
            derived_traits: args.derive.into_iter().collect(),
            macro_path: MacroPath::default(),
        })
    }
}

impl ToTokens for Definition {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.impl_convert().to_tokens(tokens);
        self.impl_bounds().to_tokens(tokens);
    }
}

impl Definition {
    /// Implements `Convert` trait for the delegated type.
    fn impl_convert(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let ident = &self.ident;

        let (impl_gens, ty_gens, where_clause) = self.generics.split_for_impl();

        let lifetime = parse_quote! { '__delegate };
        let either_owned =
            self.generate_either(self.delegated.types(), None, false);
        let either_ref = self.generate_either(
            self.delegated.types(),
            Some(&lifetime),
            false,
        );
        let either_ref_mut =
            self.generate_either(self.delegated.types(), Some(&lifetime), true);

        let mut either_where_clause: syn::WhereClause = parse_quote! { where };
        either_where_clause
            .predicates
            .extend(self.delegated.types().map(|ty| -> syn::WherePredicate {
                parse_quote! { #ty: #lifetime }
            }));

        let (convert_owned, convert_ref, convert_ref_mut) =
            match &self.delegated {
                DelegatedTypes::Variants(variants) => {
                    let arms = self.generate_match(variants);
                    (arms.clone(), arms.clone(), arms)
                }
                DelegatedTypes::Field(field) => {
                    let ident = field.ident();
                    (
                        quote! { #macro_path::Either::Left(self. #ident) },
                        quote! { #macro_path::Either::Left(&self. #ident) },
                        quote! { #macro_path::Either::Left(&mut self. #ident) },
                    )
                }
            };

        quote! {
            #[automatically_derived]
            impl #impl_gens #macro_path::Convert for #ident #ty_gens
                 #where_clause
            {
                type Owned = #either_owned;
                type Ref<#lifetime> = #either_ref #either_where_clause;
                type RefMut<#lifetime> = #either_ref_mut #either_where_clause;

                fn convert_owned(
                    self
                ) -> <Self as #macro_path::Convert>::Owned {
                    #convert_owned
                }

                fn convert_ref(
                    &self
                ) -> <Self as #macro_path::Convert>::Ref<'_> {
                    #convert_ref
                }

                fn convert_ref_mut(
                    &mut self
                ) -> <Self as #macro_path::Convert>::RefMut<'_> {
                    #convert_ref_mut
                }
            }
        }
    }

    /// Implements `Bound`s for the delegated type.
    fn impl_bounds(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let ident = &self.ident;

        self.derived_traits
            .iter()
            .cloned()
            .map(|p| {
                quote! {
                    #[automatically_derived]
                    impl #macro_path::Bound<
                        { #p .0 },
                        { #p .1 },
                        { #p .2 },
                        { #p .3 },
                    > for #ident {}
                }
            })
            .collect()
    }

    /// Generates a `Either` type like
    /// `Either<Ty1, <... Either<TyN, Void>> ...>` with optionally added maybe
    /// mutable reference before each `TyN`.
    fn generate_either<'ty, I>(
        &self,
        types: I,
        lifetime: Option<&'ty syn::Lifetime>,
        is_mutable: bool,
    ) -> TokenStream
    where
        I: IntoIterator<Item = &'ty syn::Type>,
    {
        let mut tokens = TokenStream::new();

        let macro_path = &self.macro_path;
        let either = quote! { #macro_path::Either };
        let void = quote! { #macro_path::Void };

        let mut i = 0;
        for ty in types {
            i += 1;

            either.to_tokens(&mut tokens);
            token::Lt::default().to_tokens(&mut tokens);

            if let Some(lifetime) = lifetime {
                token::And::default().to_tokens(&mut tokens);
                lifetime.to_tokens(&mut tokens);

                if is_mutable {
                    token::Mut::default().to_tokens(&mut tokens);
                }
            }

            ty.to_tokens(&mut tokens);

            token::Comma::default().to_tokens(&mut tokens);
        }

        void.to_tokens(&mut tokens);

        for _ in 0..i {
            token::Gt::default().to_tokens(&mut tokens);
        }

        tokens
    }

    /// Generates `match` expression that converts delegated types into a
    /// `Either` generated by [`Self::generate_either()`].
    ///
    /// # Example
    ///
    /// ```rust,ignore
    /// match self {
    ///     Self::Variant1(v) => Either::Left(v),
    ///     Self::Variant2 { field } =>  Either::Right(
    ///         Either::Left(field)
    ///     ),
    ///     // ...
    ///     Self::VariantN(v) => Either::Right(v),
    /// }
    /// ```
    fn generate_match(&self, variants: impl AsRef<[Variant]>) -> TokenStream {
        fn sequence(
            tokens: &mut TokenStream,
            count: usize,
            value: &syn::Ident,
            macro_path: &MacroPath,
        ) {
            let ident = if count == 0 { "Left" } else { "Right" };

            quote! { #macro_path::Either }.to_tokens(tokens);
            token::Colon2::default().to_tokens(tokens);
            syn::Ident::new(ident, Span::call_site()).to_tokens(tokens);
            token::Paren::default().surround(tokens, |tokens| {
                if count == 0 {
                    value.to_tokens(tokens);
                } else {
                    sequence(tokens, count - 1, value, macro_path);
                }
            });
        }

        let mut tokens = TokenStream::new();

        token::Match::default().to_tokens(&mut tokens);
        token::SelfValue::default().to_tokens(&mut tokens);
        token::Brace::default().surround(&mut tokens, |tokens| {
            for (i, variant) in variants.as_ref().iter().enumerate() {
                token::SelfType::default().to_tokens(tokens);
                token::Colon2::default().to_tokens(tokens);
                variant.ident.to_tokens(tokens);

                let value_ident = match &variant.field_ident {
                    Some(ident) => {
                        token::Brace::default().surround(tokens, |tokens| {
                            ident.to_tokens(tokens);
                        });
                        ident.clone()
                    }
                    None => {
                        let ident = syn::Ident::new("v", Span::call_site());
                        token::Paren::default().surround(tokens, |tokens| {
                            ident.to_tokens(tokens);
                        });
                        ident
                    }
                };

                token::FatArrow::default().to_tokens(tokens);
                token::Brace::default().surround(tokens, |tokens| {
                    sequence(tokens, i, &value_ident, &self.macro_path);
                });
            }
        });

        tokens
    }
}

/// Delegated enum's [`Variant`]s or a single struct [`Field`].
#[derive(Clone, Debug)]
enum DelegatedTypes {
    /// [`Variant`]s of the enum.
    Variants(Vec<Variant>),

    /// [`Field`] of the struct.
    Field(Field),
}

impl DelegatedTypes {
    /// Returns an [`Iterator`] over these [`DelegatedTypes`].
    fn types(&self) -> impl Iterator<Item = &syn::Type> {
        use itertools::Either::{Left, Right};

        match self {
            Self::Variants(variants) => {
                Left(variants.iter().map(|var| &var.ty))
            }
            Self::Field(field) => Right(iter::once(field.ty())),
        }
    }
}

/// Field of a struct.
#[derive(Clone, Debug)]
enum Field {
    /// [`Field`] of named struct.
    Named {
        ident: syn::Ident,
        ty: Box<syn::Type>,
    },

    /// [`Field`] of tuple struct.
    Unnamed {
        index: syn::Index,
        ty: Box<syn::Type>,
    },
}

impl Field {
    /// Returns an [`Ident`] or an [`Index`] to access this [`Field`].
    ///
    /// [`Ident`]: syn::Ident
    /// [`Index`]: syn::Index
    fn ident(&self) -> TokenStream {
        match self {
            Self::Named { ident, .. } => ident.to_token_stream(),
            Self::Unnamed { index, .. } => index.to_token_stream(),
        }
    }

    /// Returns a [`Type`] of this [`Field`].
    ///
    /// [`Type`]: syn::Type
    fn ty(&self) -> &syn::Type {
        match self {
            Self::Named { ty, .. } | Self::Unnamed { ty, .. } => ty,
        }
    }
}

impl TryFrom<syn::Fields> for Field {
    type Error = syn::Error;

    fn try_from(fields: syn::Fields) -> Result<Self, Self::Error> {
        fields
            .iter()
            .at_most_one()
            .ok()
            .flatten()
            .map(|f| {
                f.ident.as_ref().map_or_else(
                    || Self::Unnamed {
                        index: syn::Index {
                            index: 0,
                            span: f.span(),
                        },
                        ty: Box::new(f.ty.clone()),
                    },
                    |ident| Self::Named {
                        ident: ident.clone(),
                        ty: Box::new(f.ty.clone()),
                    },
                )
            })
            .ok_or_else(|| {
                syn::Error::new(
                    fields.span(),
                    "struct must have exactly one field",
                )
            })
    }
}

/// Variant of an enum.
#[derive(Clone, Debug)]
struct Variant {
    /// [`Ident`] of this [`Variant`].
    ///
    /// [`Ident`]: syn::Ident
    ident: syn::Ident,

    /// [`Ident`] of the field contained in this variant.
    ///
    /// [`None`] means variant field is unnamed.
    ///
    /// [`Ident`]: syn::Ident
    field_ident: Option<syn::Ident>,

    /// [`Type`] of this [`Variant`].
    ///
    /// [`Type`]: syn::Type
    ty: syn::Type,
}

impl TryFrom<syn::Variant> for Variant {
    type Error = syn::Error;

    fn try_from(varint: syn::Variant) -> Result<Self, Self::Error> {
        varint
            .fields
            .iter()
            .at_most_one()
            .ok()
            .flatten()
            .map(|f| Self {
                ident: varint.ident,
                field_ident: f.ident.clone(),
                ty: f.ty.clone(),
            })
            .ok_or_else(|| {
                syn::Error::new(
                    varint.fields.span(),
                    "enum variant must have exactly one field",
                )
            })
    }
}

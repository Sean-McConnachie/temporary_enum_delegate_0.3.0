//! `#[delegate]` macro expansion on traits.

mod utils;

use proc_macro2::TokenStream;
use quote::{format_ident, quote, ToTokens};
use syn::{
    parse::{Parse, ParseStream},
    parse_quote,
    punctuated::Punctuated,
    spanned::Spanned as _,
    token,
};

use crate::MacroPath;

use self::utils::SignatureExt as _;

/// Arguments for `#[delegate]` macro expansion on traits.
struct Args {
    /// `for` attribute argument, specifying types for deriving.
    r#for: Punctuated<syn::Path, token::Comma>,
}

impl Parse for Args {
    fn parse(input: ParseStream<'_>) -> syn::Result<Self> {
        if input.is_empty() {
            return Ok(Self {
                r#for: Punctuated::new(),
            });
        }

        let _ = input.parse::<token::For>().map_err(|e| {
            syn::Error::new(e.span(), "expected `for` attribute argument")
        })?;

        let args;
        let _ = syn::parenthesized!(args in input);
        Ok(Self {
            r#for: Punctuated::parse_terminated(&args)?,
        })
    }
}

/// Definitions of `#[delegate]` macro expansion on traits.
#[derive(Debug)]
pub(super) struct Definition {
    /// [`Visibility`] of the trait.
    ///
    /// [`Visibility`]: syn::Visibility
    vis: syn::Visibility,

    /// Indicator whether the trait is unsafe.
    unsafety: Option<syn::token::Unsafe>,

    /// [`Ident`] of the trait.
    ///
    /// [`Ident`]: syn::Ident
    ident: syn::Ident,

    /// [`Generics`] of the trait.
    ///
    /// [`Generics`]: syn::Generics
    generics: syn::Generics,

    /// Methods with `self` receiver.
    methods_owned: Vec<syn::TraitItemMethod>,

    /// Methods with `&self` receiver.
    methods_ref: Vec<syn::TraitItemMethod>,

    /// Methods with `&mut self` receiver.
    methods_ref_mut: Vec<syn::TraitItemMethod>,

    /// Types for deriving trait on.
    delegate_for: Vec<syn::Path>,

    /// [`Ident`] for generated trait, that contains only methods with `self`
    /// receiver.
    ///
    /// [`Ident`]: syn::Ident
    owned_trait_ident: syn::Ident,

    /// [`Ident`] for generated trait, that contains only methods with `&self`
    /// receiver.
    ///
    /// [`Ident`]: syn::Ident
    ref_trait_ident: syn::Ident,

    /// [`Ident`] for generated trait, that contains only methods with
    /// `&mut self` receiver.
    ///
    /// [`Ident`]: syn::Ident
    ref_mut_trait_ident: syn::Ident,

    /// Path to the macro definitions.
    macro_path: MacroPath,
}

impl Definition {
    /// Parses [`Definition`] from a [`syn::ItemTrait`].
    pub(super) fn parse(
        mut item: syn::ItemTrait,
        args: TokenStream,
    ) -> syn::Result<Self> {
        let args = syn::parse2::<Args>(args)?;

        let mut methods_owned = Vec::new();
        let mut methods_ref = Vec::new();
        let mut methods_ref_mut = Vec::new();

        for item in &item.items {
            match item {
                syn::TraitItem::Method(m) => match m.sig.receiver() {
                    Some(syn::FnArg::Receiver(syn::Receiver {
                        reference: Some(_),
                        mutability: Some(_),
                        ..
                    })) => methods_ref_mut.push(m.clone()),
                    Some(syn::FnArg::Receiver(syn::Receiver {
                        reference: Some(_),
                        mutability: None,
                        ..
                    })) => methods_ref.push(m.clone()),
                    Some(syn::FnArg::Receiver(syn::Receiver {
                        reference: None,
                        ..
                    })) => methods_owned.push(m.clone()),
                    Some(syn::FnArg::Typed(_)) | None => {
                        Err(syn::Error::new(
                            m.span(),
                            "all trait method must have an untyped receiver",
                        ))?;
                    }
                },
                item @ (syn::TraitItem::Type(_)
                | syn::TraitItem::Const(_)
                | syn::TraitItem::Macro(_)
                | syn::TraitItem::Verbatim(_)) => Err(syn::Error::new(
                    item.span(),
                    "only trait methods with untyped receiver are allowed",
                ))?,
                // TODO: Use `non_exhaustive_omitted_patterns`, once stabilized.
                //       https://github.com/rust-lang/rust/issues/89554
                // #[cfg_attr(test, deny(non_exhaustive_omitted_patterns))]
                item => panic!("{item:#?} not covered"),
            }
        }

        item.generics.make_where_clause().predicates.extend(
            item.supertraits.iter().map(|t| -> syn::WherePredicate {
                parse_quote! { Self: #t }
            }),
        );

        let owned_trait_ident = format_ident!("{}__DelegateOwned", item.ident);
        let ref_trait_ident = format_ident!("{}__DelegateRef", item.ident);
        let ref_mut_trait_ident =
            format_ident!("{}__DelegateRefMut", item.ident);

        Ok(Self {
            vis: item.vis,
            unsafety: item.unsafety,
            ident: item.ident,
            generics: item.generics,
            methods_owned,
            methods_ref,
            methods_ref_mut,
            delegate_for: args.r#for.into_iter().collect(),
            owned_trait_ident,
            ref_trait_ident,
            ref_mut_trait_ident,
            macro_path: MacroPath::default(),
        })
    }
}

impl ToTokens for Definition {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.generate_bound_const().to_tokens(tokens);

        // Expand everything else inside `__delegate_{TraitName}` module to
        // avoid accidental leaks.
        quote! { #[allow(non_snake_case)] }.to_tokens(tokens);
        quote! { #[automatically_derived] }.to_tokens(tokens);
        quote! { #[doc(hidden)] }.to_tokens(tokens);
        token::Mod::default().to_tokens(tokens);
        format_ident!("__delegate_{}", self.ident).to_tokens(tokens);
        token::Brace::default().surround(tokens, |tokens| {
            quote! { use super::*; }.to_tokens(tokens);

            self.generate_owned_trait().to_tokens(tokens);
            self.impl_owned_trait_for_either().to_tokens(tokens);
            self.impl_owned_trait_for_void().to_tokens(tokens);

            self.generate_ref_trait(false).to_tokens(tokens);
            self.impl_ref_trait_for_either(false).to_tokens(tokens);
            self.impl_ref_trait_for_void(false).to_tokens(tokens);

            self.generate_ref_trait(true).to_tokens(tokens);
            self.impl_ref_trait_for_either(true).to_tokens(tokens);
            self.impl_ref_trait_for_void(true).to_tokens(tokens);

            self.blanket_impl_for_delegated_trait().to_tokens(tokens);

            self.impl_bounds().to_tokens(tokens);

            self.generate_self_bound_assertions().to_tokens(tokens);
        });
    }
}

impl Definition {
    /// Generates a trait containing only methods with `self` receiver.
    fn generate_owned_trait(&self) -> TokenStream {
        let owned_trait = &self.owned_trait_ident;
        let generics = &self.generics;
        let where_clause = &self.generics.where_clause;
        let owned_methods = &self.methods_owned;

        quote! {
            #[allow(non_camel_case_types)]
            #[automatically_derived]
            trait #owned_trait #generics #where_clause {
                #( #owned_methods )*
            }
        }
    }

    /// Implements a trait generated by [`Self::generate_owned_trait()`] for a
    /// `Either`.
    fn impl_owned_trait_for_either(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let orig_trait = &self.ident;
        let owned_trait = &self.owned_trait_ident;

        let (_, ty_gens, _) = self.generics.split_for_impl();

        let generics = {
            let mut gens = self.generics.clone();

            let params: [syn::GenericParam; 2] =
                [parse_quote! { __Left }, parse_quote! { __Right }];
            gens.params.extend(params);

            let predicates: [syn::WherePredicate; 2] = [
                parse_quote! { __Left: #orig_trait #ty_gens },
                parse_quote! { __Right: #owned_trait #ty_gens },
            ];
            gens.make_where_clause().predicates.extend(predicates);

            gens
        };
        let (impl_gens, _, where_clause) = generics.split_for_impl();

        let methods = self.methods_owned.iter().map(|m| {
            let (signature, method_name, method_inputs) =
                m.sig.split_for_impl();
            let method_inputs = method_inputs.collect::<Vec<_>>();

            quote! {
                #signature {
                    match self {
                        Self::Left(__delegate) => {
                            <__Left as #orig_trait #ty_gens>::#method_name(
                                __delegate, #( #method_inputs ),*
                            )
                        }
                        Self::Right(__delegate) => {
                            <__Right as #owned_trait #ty_gens>::#method_name(
                                __delegate, #( #method_inputs ),*
                            )
                        }
                    }
                }
            }
        });

        quote! {
            #[automatically_derived]
            impl #impl_gens #owned_trait #ty_gens
             for #macro_path::Either<__Left, __Right> #where_clause
            {
                #( #methods )*
            }
        }
    }

    /// Implements a trait generated by [`Self::generate_owned_trait()`] for a
    /// `Void`.
    fn impl_owned_trait_for_void(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let owned_trait = &self.owned_trait_ident;

        let (impl_gens, ty_gens, where_clause) = self.generics.split_for_impl();

        let methods = self.methods_owned.iter().map(|m| {
            let signature = &m.sig;

            quote! {
                #signature {
                    match self {}
                }
            }
        });

        quote! {
            #[automatically_derived]
            impl #impl_gens #owned_trait #ty_gens
             for #macro_path::Void #where_clause
            {
                #( #methods )*
            }
        }
    }

    /// Generates a trait containing only methods with `&self` (or `&mut self`)
    /// receiver.
    fn generate_ref_trait(&self, mutable: bool) -> TokenStream {
        let ref_trait = if mutable {
            &self.ref_mut_trait_ident
        } else {
            &self.ref_trait_ident
        };

        let generics = self.ref_trait_generics();
        let where_clause = &generics.where_clause;

        let methods = self.ref_trait_signatures(mutable);

        quote! {
            #[allow(non_camel_case_types)]
            #[automatically_derived]
            trait #ref_trait #generics #where_clause {
                #( #methods; )*
            }
        }
    }

    /// Implements a trait generated by [`Self::generate_ref_trait()`] for a
    /// `Either`.
    fn impl_ref_trait_for_either(&self, mutable: bool) -> TokenStream {
        let mut_ = mutable.then(|| quote! { mut });
        let macro_path = &self.macro_path;
        let orig_trait = &self.ident;
        let ref_trait = if mutable {
            &self.ref_mut_trait_ident
        } else {
            &self.ref_trait_ident
        };

        let (_, trait_ty_gens, _) = self.generics.split_for_impl();

        let ref_trait_generics = self.ref_trait_generics();
        let (_, ref_trait_ty_gens, _) = ref_trait_generics.split_for_impl();

        let impl_generics = {
            let mut gens = ref_trait_generics.clone();

            let params: [syn::GenericParam; 2] =
                [parse_quote! { __Left }, parse_quote! { __Right }];
            gens.params.extend(params);

            let predicates: [syn::WherePredicate; 2] = [
                parse_quote! { __Left: #orig_trait #trait_ty_gens },
                parse_quote! { __Right: #ref_trait #ref_trait_ty_gens },
            ];
            gens.make_where_clause().predicates.extend(predicates);

            gens
        };
        let (impl_gens, _, where_clause) = impl_generics.split_for_impl();

        let methods = self.ref_trait_signatures(mutable).map(|signature| {
            let (signature, method_name, method_inputs) =
                signature.split_for_impl();
            let method_inputs = method_inputs.collect::<Vec<_>>();

            quote! {
                #signature {
                    match self {
                        Self::Left(__delegate) => {
                            <__Left as #orig_trait #trait_ty_gens>
                            ::#method_name(
                                __delegate, #( #method_inputs ),*
                            )
                        }
                        Self::Right(__delegate) => {
                            <__Right as #ref_trait #ref_trait_ty_gens>
                            ::#method_name(
                                __delegate, #( #method_inputs ),*
                            )
                        }
                    }
                }
            }
        });

        quote! {
            #[automatically_derived]
            impl #impl_gens #ref_trait #ref_trait_ty_gens
             for #macro_path::Either<&'__delegate #mut_ __Left, __Right>
                 #where_clause
            {
                #( #methods )*
            }
        }
    }

    /// Implements a trait generated by [`Self::generate_ref_trait()`] for a
    /// `Void`.
    fn impl_ref_trait_for_void(&self, mutable: bool) -> TokenStream {
        let macro_path = &self.macro_path;
        let ref_trait = if mutable {
            &self.ref_mut_trait_ident
        } else {
            &self.ref_trait_ident
        };

        let generics = self.ref_trait_generics();
        let (impl_gens, ty_gens, where_clause) = generics.split_for_impl();

        let methods = self.ref_trait_signatures(mutable).map(|signature| {
            quote! {
                #signature {
                    match self {}
                }
            }
        });

        quote! {
            #[automatically_derived]
            impl #impl_gens #ref_trait #ty_gens
             for #macro_path::Void #where_clause
            {
                #( #methods )*
            }
        }
    }

    /// Generates a blanket impl of delegated trait for any type that implements
    /// `Convert*` traits, where its associated types satisfy the corresponding
    /// generated traits.
    fn blanket_impl_for_delegated_trait(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let unsafety = self.unsafety;
        let trait_ident = &self.ident;

        let owned_ident = &self.owned_trait_ident;
        let ref_ident = &self.ref_trait_ident;
        let ref_mut_ident = &self.ref_mut_trait_ident;

        let for_ty = quote! { __Delegate };

        let (_, trait_ty_gens, _) = self.generics.split_for_impl();

        let ref_trait_generics = self.ref_trait_generics();
        let (_, ref_trait_ty_gens, _) = ref_trait_generics.split_for_impl();

        let ref_trait_anon_generics = {
            let mut gens = self.generics.clone();
            gens.params.push(parse_quote! { '_ });
            gens
        };
        let (_, ref_trait_anon_ty_gens, _) =
            ref_trait_anon_generics.split_for_impl();

        let impl_generics = {
            let mut gens = self.generics.clone();

            gens.params.push(parse_quote! { #for_ty });

            let predicates: [syn::WherePredicate; 4] = [
                parse_quote! {
                    #for_ty: #macro_path::Convert
                        + #macro_path::Bound<
                            { #trait_ident .0 },
                            { #trait_ident .1 },
                            { #trait_ident .2 },
                            { #trait_ident .3 },
                        >
                },
                parse_quote! {
                    <#for_ty as #macro_path::Convert>::Owned:
                        #owned_ident #trait_ty_gens
                },
                parse_quote! {
                    for<'__delegate>
                    <#for_ty as #macro_path::Convert>::Ref<'__delegate>:
                        #ref_ident #ref_trait_ty_gens
                },
                parse_quote! {
                    for<'__delegate>
                    <#for_ty as #macro_path::Convert>::RefMut<'__delegate>:
                        #ref_mut_ident #ref_trait_ty_gens
                },
            ];
            gens.make_where_clause().predicates.extend(predicates);

            gens
        };
        let (impl_gens, _, where_clause) = impl_generics.split_for_impl();

        let owned_methods = self.methods_owned.iter().map(|m| {
            let (signature, method_name, method_inputs) =
                m.sig.split_for_impl();

            quote! {
                #signature {
                    <<#for_ty as #macro_path::Convert>::Owned
                     as #owned_ident>
                    ::#method_name(
                        <Self as #macro_path::Convert>::convert_owned(self),
                        #( #method_inputs ),*
                    )
                }
            }
        });
        let ref_methods = self.methods_ref.iter().map(|m| {
            let (signature, method_name, method_inputs) =
                m.sig.split_for_impl();

            quote! {
                #signature {
                    <<#for_ty as #macro_path::Convert>::Ref<'_>
                     as #ref_ident #ref_trait_anon_ty_gens>
                    ::#method_name(
                        <Self as #macro_path::Convert>::convert_ref(self),
                        #( #method_inputs ),*
                    )
                }
            }
        });
        let ref_mut_methods = self.methods_ref_mut.iter().map(|m| {
            let (signature, method_name, method_inputs) =
                m.sig.split_for_impl();

            quote! {
                #signature {
                    <<#for_ty as #macro_path::Convert>::RefMut<'_>
                     as #ref_mut_ident #ref_trait_anon_ty_gens>
                    ::#method_name(
                        <Self as #macro_path::Convert>::convert_ref_mut(self),
                        #( #method_inputs ),*
                    )
                }
            }
        });

        quote! {
            #[automatically_derived]
            #unsafety impl #impl_gens #trait_ident #trait_ty_gens for #for_ty
                 #where_clause
            {
                #( #owned_methods )*
                #( #ref_methods )*
                #( #ref_mut_methods )*
            }
        }
    }

    /// Generates constant, that uniquely identifies delegated trait.
    fn generate_bound_const(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let vis = &self.vis;
        let ident = &self.ident;
        let ident_str = ident.to_string();

        quote! {
            #[allow(non_upper_case_globals)]
            #[automatically_derived]
            #[doc(hidden)]
            #vis const #ident: (u128, u128, u32, u32) = (
                #macro_path::fnv1a128(::core::file!()),
                #macro_path::fnv1a128(#ident_str),
                ::core::line!(),
                ::core::column!(),
            );
        }
    }

    /// Implements a `Bound` trait with constants generated by
    /// [`Self::generate_bound_const()`] for types, that delegate the trait.
    fn impl_bounds(&self) -> TokenStream {
        let macro_path = &self.macro_path;
        let ident = &self.ident;

        self.delegate_for
            .iter()
            .cloned()
            .map(|p| {
                quote! {
                    #[automatically_derived]
                    impl #macro_path::Bound<
                        { #ident .0 },
                        { #ident .1 },
                        { #ident .2 },
                        { #ident .3 }
                    > for #p {}
                }
            })
            .collect()
    }

    // TODO: Add proper support for `Self:` bounds.
    /// Generates assertion, that `Self:` bounds contain only marker traits like
    /// [`Sized`], [`Send`] or [`Sync`].
    fn generate_self_bound_assertions(&self) -> TokenStream {
        /// Returns [`Iterator`] of [`syn::TraitBound`] for `Self:`.
        fn self_trait_bounds(
            pred: &syn::WherePredicate,
        ) -> impl Iterator<Item = &syn::TraitBound> {
            let self_: syn::Type = parse_quote! { Self };

            match pred {
                syn::WherePredicate::Type(syn::PredicateType {
                    bounded_ty,
                    bounds,
                    ..
                }) => (bounded_ty == &self_).then(|| {
                    bounds.iter().filter_map(|b| match b {
                        syn::TypeParamBound::Trait(tr) => Some(tr),
                        syn::TypeParamBound::Lifetime(_) => None,
                    })
                }),
                syn::WherePredicate::Lifetime(_)
                | syn::WherePredicate::Eq(_) => None,
            }
            .into_iter()
            .flatten()
        }

        let self_bounds = self
            .generics
            .where_clause
            .iter()
            .flat_map(|cl| cl.predicates.iter())
            .flat_map(self_trait_bounds);

        quote! {
            #[automatically_derived]
            const _: fn() = || {
                struct OnlyMarkerSelfBoundsSupportedForNow;

                fn assert_impl_all<T: Sized #(+ #self_bounds )*>() {}
                assert_impl_all::<OnlyMarkerSelfBoundsSupportedForNow>();
            };
        }
    }

    /// Returns an [`Iterator`] over methods with `&self` or `&mut self`
    /// receivers with [lifted] lifetimes.
    ///
    /// [lifted]: utils::SignatureExt::lift_receiver_lifetime()
    fn ref_trait_signatures(
        &self,
        mutable: bool,
    ) -> impl Iterator<Item = syn::Signature> + '_ {
        if mutable {
            self.methods_ref_mut.iter()
        } else {
            self.methods_ref.iter()
        }
        .cloned()
        .map(|mut method| {
            method
                .sig
                .lift_receiver_lifetime(parse_quote! { '__delegate });
            method.sig
        })
    }

    /// Returns [`syn::Generics`] for traits generated by
    /// [`Self::generate_ref_trait()`].
    fn ref_trait_generics(&self) -> syn::Generics {
        let mut gens = self.generics.clone();
        gens.params.push(parse_quote! { '__delegate });
        gens.make_where_clause()
            .predicates
            .push(parse_quote! { Self: Sized + '__delegate });
        gens
    }
}

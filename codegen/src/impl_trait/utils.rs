//! Utils for `#[delegate]` macro expansion on traits.

use std::{collections::HashSet, iter, mem};

use syn::{
    parse_quote, punctuated,
    visit_mut::{self, VisitMut},
};

/// Extension of a [`syn::Signature`].
pub(super) trait SignatureExt {
    /// Helper for implementing method on a `Either`.
    ///
    /// Returns method's [`Signature`] itself, [`Ident`] and [`Iterator`] over
    /// inputs, excluding the [`Receiver`].
    ///
    /// [`Ident`]: syn::Ident
    /// [`Receiver`]: syn::Receiver
    /// [`Signature`]: syn::Signature
    fn split_for_impl(
        &self,
    ) -> (&syn::Signature, &syn::Ident, InputsWithoutReceiverIter<'_>);

    /// Lifts [`Lifetime`] from a [`Receiver`] leaving `self` in its place and
    /// replaces/inserts all the needed occurrences with the provided
    /// `replace_with` [`Lifetime`].
    ///
    /// This method is useful, when you need lift [`Receiver`]'s [`Lifetime`] to
    /// trait/struct generic level.
    ///
    /// [`Lifetime`]: syn::Lifetime
    /// [`Receiver`]: syn::Receiver
    fn lift_receiver_lifetime(&mut self, replace_with: syn::Lifetime);
}

impl SignatureExt for syn::Signature {
    fn split_for_impl(
        &self,
    ) -> (&syn::Signature, &syn::Ident, InputsWithoutReceiverIter<'_>) {
        (
            self,
            &self.ident,
            self.inputs.iter().filter_map(|i| match i {
                syn::FnArg::Typed(t) => Some(&t.pat),
                syn::FnArg::Receiver(_) => None,
            }),
        )
    }

    fn lift_receiver_lifetime(&mut self, replace_with: syn::Lifetime) {
        /// Replacer of the `replaced` [`Lifetime`] with the `replace_with` one.
        ///
        /// [`Lifetime`]: syn::Lifetime
        struct ReplaceLifetimes {
            /// [`Lifetime`] to be replaced.
            ///
            /// [`Lifetime`]: syn::Lifetime
            replaced: HashSet<syn::Lifetime>,

            /// [`Lifetime`] to replace with.
            ///
            /// [`Lifetime`]: syn::Lifetime
            replace_with: syn::Lifetime,
        }

        impl VisitMut for ReplaceLifetimes {
            fn visit_lifetime_mut(&mut self, l: &mut syn::Lifetime) {
                if self.replaced.contains(l) {
                    *l = self.replace_with.clone();
                }
            }
        }

        /// Inserter of a [`Lifetime`] after every empty reference.
        ///
        /// [`Lifetime`]: syn::Lifetime
        struct InsertLifetime {
            /// [`Lifetime`] to be inserted.
            ///
            /// [`Lifetime`]: syn::Lifetime
            inserted: syn::Lifetime,
        }

        impl VisitMut for InsertLifetime {
            fn visit_type_reference_mut(
                &mut self,
                ty: &mut syn::TypeReference,
            ) {
                if ty.lifetime.is_none() {
                    ty.lifetime = Some(self.inserted.clone());
                }

                visit_mut::visit_type_reference_mut(self, ty);
            }
        }

        // 1. Remove lifetime from receiver or return, if there is no reference.
        let self_lifetime = match self.inputs.first_mut() {
            Some(syn::FnArg::Receiver(rec)) => {
                if rec.reference.is_none() {
                    return;
                }

                rec.mutability = None;
                rec.reference.take().and_then(|(_, l)| l)
            }
            Some(syn::FnArg::Typed(_)) | None => return,
        };

        // 2. Replace receiver's lifetime and `'_` with `replace_with` in
        //    signature's output.
        let mut replacer = ReplaceLifetimes {
            replaced: [self_lifetime, Some(parse_quote! { '_ })]
                .into_iter()
                .flatten()
                .collect(),
            replace_with,
        };
        replacer.visit_return_type_mut(&mut self.output);

        // 3. Replace receiver's lifetime with `replace_with` in entire
        //    signature.
        let _ = replacer.replaced.remove(&parse_quote! { '_ });
        if !replacer.replaced.is_empty() {
            replacer.visit_signature_mut(self);
        }

        // 4. Remove replace_with` lifetime from generic parameters.
        let generic_params_len_before = self.generics.params.len();
        self.generics.params = mem::take(&mut self.generics.params)
            .into_iter()
            .filter(|par| match par {
                syn::GenericParam::Lifetime(syn::LifetimeDef {
                    lifetime,
                    ..
                }) if lifetime == &replacer.replace_with => false,
                syn::GenericParam::Lifetime(_)
                | syn::GenericParam::Type(_)
                | syn::GenericParam::Const(_) => true,
            })
            .collect();

        // 5. In case `self_lifetime` came from trait generics, we add bound
        //    `#replace_with: #self_lifetime` and
        //    `#self_lifetime: #replace_with` to indicate, that they are
        //    identical.
        if let Some(self_lifetime) = replacer.replaced.iter().next() {
            if self.generics.params.len() == generic_params_len_before {
                let replace_with = &replacer.replace_with;
                let predicates: [syn::WherePredicate; 2] = [
                    parse_quote! { #self_lifetime: #replace_with },
                    parse_quote! { #replace_with: #self_lifetime },
                ];
                self.generics
                    .make_where_clause()
                    .predicates
                    .extend(predicates);
            }
        }

        // 6. Insert `replace_with` after every reference without a lifetime in
        //    signature's output.
        InsertLifetime {
            inserted: replacer.replace_with,
        }
        .visit_return_type_mut(&mut self.output);
    }
}

/// [`Iterator`] over [`Signature`]'s inputs, excluding its [`Receiver`].
///
/// [`Receiver`]: syn::Receiver
/// [`Signature`]: syn::Signature
pub(super) type InputsWithoutReceiverIter<'a> = iter::FilterMap<
    punctuated::Iter<'a, syn::FnArg>,
    fn(&'a syn::FnArg) -> Option<&'a Box<syn::Pat>>,
>;

#[cfg(test)]
mod lift_self_lifetime_spec {
    use quote::{quote, ToTokens as _};
    use syn::parse_quote;

    use super::SignatureExt;

    fn with() -> syn::Lifetime {
        parse_quote! { '__delegated }
    }

    #[test]
    fn without_receiver() {
        for (input, expected) in [(
            parse_quote! {
                fn test<'a, 'b>(arg1: &'a i32, arg2: &'b i32) -> &'a i32
            },
            quote! {
                fn test<'a, 'b>(arg1: &'a i32, arg2: &'b i32) -> &'a i32
            },
        )] {
            let mut input: syn::Signature = input;
            input.lift_receiver_lifetime(with());

            assert_eq!(
                input.to_token_stream().to_string(),
                expected.to_string(),
            );
        }
    }

    #[test]
    fn receiver_without_reference() {
        for (input, expected) in [(
            parse_quote! {
                fn test<'a>(self, arg2: &'a i32) -> &'a i32
            },
            quote! {
                fn test<'a>(self, arg2: &'a i32) -> &'a i32
            },
        )] {
            let mut input: syn::Signature = input;
            input.lift_receiver_lifetime(with());

            assert_eq!(
                input.to_token_stream().to_string(),
                expected.to_string(),
            );
        }
    }

    #[test]
    fn receiver_without_lifetime() {
        for (input, expected) in [
            (
                parse_quote! {
                    fn test<'a>(&self, arg2: &'a i32) -> &i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&self, arg2: &'a i32) -> &'_ i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&self, arg2: &'a i32) -> &'a i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'a i32
                },
            ),
        ] {
            let mut input: syn::Signature = input;
            input.lift_receiver_lifetime(with());

            assert_eq!(
                input.to_token_stream().to_string(),
                expected.to_string(),
            );
        }
    }

    #[test]
    fn receiver_with_anonymous_lifetime() {
        for (input, expected) in [
            (
                parse_quote! {
                    fn test<'a>(&'_ self, arg2: &'a i32) -> &i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&'_ self, arg2: &'a i32) -> &'_ i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&'_ self, arg2: &'a i32) -> &'a i32
                },
                quote! {
                    fn test<'a>(self, arg2: &'a i32) -> &'a i32
                },
            ),
            (
                parse_quote! {
                    fn test(&'_ self, arg2: &'_ i32) -> &i32
                },
                quote! {
                    fn test(self, arg2: &'_ i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test(&'_ self, arg2: &'_ i32) -> &'_ i32
                },
                quote! {
                    fn test(self, arg2: &'_ i32) -> &'__delegated i32
                },
            ),
        ] {
            let mut input: syn::Signature = input;
            input.lift_receiver_lifetime(with());

            assert_eq!(
                input.to_token_stream().to_string(),
                expected.to_string(),
            );
        }
    }

    #[test]
    fn receiver_with_lifetime() {
        for (input, expected) in [
            (
                parse_quote! {
                    fn test<'a, 'b>(&'a self, arg2: &'b i32) -> &'a i32
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a, 'b>(&'a self, arg2: &'b i32) -> &'b i32
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'b i32
                },
            ),
            (
                parse_quote! {
                    fn test<'b>(&'a self, arg2: &'b i32) -> &'a i32
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'__delegated i32
                    where
                        'a: '__delegated,
                        '__delegated: 'a
                },
            ),
            (
                parse_quote! {
                    fn test<'b>(&'a self, arg2: &'b i32) -> &'b i32
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'b i32
                    where
                        'a: '__delegated,
                        '__delegated: 'a
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&'a self, arg2: &'b i32) -> &'b i32
                },
                quote! {
                    fn test(self, arg2: &'b i32) -> &'b i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a>(&'a self, arg2: &'b i32) -> &'a i32
                },
                quote! {
                    fn test(self, arg2: &'b i32) -> &'__delegated i32
                },
            ),
            (
                parse_quote! {
                    fn test<'a, 'b>(&'a self, arg2: &'b i32) -> &'a i32
                    where
                        'a: 'b
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'__delegated i32
                    where
                        '__delegated: 'b
                },
            ),
            (
                parse_quote! {
                    fn test<'b>(&'a self, arg2: &'b i32) -> &'a i32
                    where
                        'a: 'b
                },
                quote! {
                    fn test<'b>(self, arg2: &'b i32) -> &'__delegated i32
                    where
                        '__delegated: 'b,
                        'a: '__delegated,
                        '__delegated: 'a
                },
            ),
        ] {
            let mut input: syn::Signature = input;
            input.lift_receiver_lifetime(with());

            assert_eq!(
                input.to_token_stream().to_string(),
                expected.to_string(),
            );
        }
    }
}

use enum_delegate::delegate;

#[delegate]
trait AsStr {
    fn as_str(&self) -> &str;
}

#[delegate(derive(AsStr))]
struct FirstName(String);

fn main() {
    let name = FirstName("John".to_string());
    assert_eq!(name.as_str(), "John");
}

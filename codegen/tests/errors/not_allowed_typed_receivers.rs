use enum_delegate::delegate;

#[delegate]
trait Channel {
    fn id(this: &Self) -> usize;
}

fn main() {
    unreachable!()
}

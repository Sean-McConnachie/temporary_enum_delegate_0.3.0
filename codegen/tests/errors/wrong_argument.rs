use enum_delegate::delegate;

#[delegate(derive(AsStr))]
trait AsStr {
    fn as_str(&self) -> &str;
}

#[delegate(for(AsStr))]
struct FirstName(String);

fn main() {
    unreachable!()
}

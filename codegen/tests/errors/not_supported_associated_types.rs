// TODO: Remove once associated types are supported.

use enum_delegate::delegate;

#[delegate]
trait Parser {
    type Buffer;
}

fn main() {
    unreachable!()
}
